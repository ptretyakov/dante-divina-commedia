const fs = require('fs');
const path = require('path');
const https = require('https');
const express = require('express');

//const chain = fs.readFileSync('sslcert/intermediate.crt', 'utf8');
const privateKey  = fs.readFileSync('sslcert/private.key', 'utf8');
const certificate = fs.readFileSync('sslcert/cert.crt', 'utf8');

console.log('TEEEEST =>', privateKey);

//const credentials = {cert: certificate};
const credentials = {key: privateKey, cert: certificate};
const app = express();
const port = process.env.PORT || 8001;

const httpsServer = https.createServer(credentials, app);

app.use(express.static(path.join(__dirname, 'build')));

httpsServer.listen(port, () => {
  console.log(`Dante Divina Commedia app listening at https://localhost:${port}`);
});
