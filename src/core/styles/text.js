import tw from 'tailwind.macro';
import React from 'react';
import styled from 'styled-components';

export const TextSubtitle = styled.h3`
  font-size: 35px;
  font-weight: 600;
  ${tw`mt-10 mb-7`}
`;

export const BigWhiteTitle = styled.h1`
  color: #fff;
  font-size: ${p => {
    console.log('TEST', p.size);
    return  p.size === 'normal' ? '157px' : '180px';
  }};
  line-height: 180px;
  text-shadow: 0px 0px 10px #000000;
`;

export const ImageSubtitle = styled.h3`
  color: #fff;
  font-size: 70px;
  margin-top: 13px;
  text-shadow: 0px 0px 10px #000000;
`;

export const TextCenter = styled.div`
  text-align: center;
`;

export const Quote = styled(({text = '', signature = '', ...props }) => (
  <div {...props}>
    <blockquote>
      {text.split('\n').map((str, index, arr) => (
        <p>
          {(index === 0) && '"'}
          {str}
          {(index === arr.length - 1) && '"'}
        </p>
      ))}
    </blockquote>
    <p>{signature}</p>
  </div>
))`
  blockquote {
    color: red;
    font-size: 24pt;
    font-style: italic;
    line-height: 25px;
  }

  & > p {
    font-size: 18pt;
    margin-top: 13px !important;
  }
`;