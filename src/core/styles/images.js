import React from 'react';

import tw from 'tailwind.macro';
import styled from 'styled-components';

export const FullscreenSingleCenterImage = styled.div`
  height: calc(100vh - 140px);
  padding-top: 140px;
  background-image: url(${p => p.image});
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
`;

export const FullscreenImage = styled.div`
  height: calc(100vh - 140px);
  padding-top: 140px;
  background-size: cover;
  background-image: url(${p => p.image});
`;

export const FullWidthImage = styled.div`
  padding-top: 140px;
  background-image: url(${p => p.image});
  background-size: contain;
  background-repeat: no-repeat;
`;

export const TextImage = styled.img`
  ${tw`mt-10 mb-10`}
  width: 100%;
  border: 1px solid #000;
`

export const ImageWithSignature = styled(
  ({ image, signature, withoutBorder, ...props }) => (
    <figure {...props}>
      <img src={image} alt={signature} />
      <figcaption>
        {props.children ? props.children : signature}
      </figcaption>
    </figure>
  )
)`
  ${tw`mt-10 mb-10`}
  img {
    width: 100%;
    border: ${p => !p.withoutBorder ? '1px' : '0px'} solid #000;
    ${p => {
      if(p.withShadow) return 'box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);'
    }}
  }

  figcaption {
    font-size: 18pt;
  }
`;