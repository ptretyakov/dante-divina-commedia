import React from 'react';
import tw from 'tailwind.macro';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

import logoImage from './assets/logo.jpg';

export const Header = () => {
  return (
    <StyledHeader>
      <NavLink to="/">
        <img src={logoImage} alt="" />
      </NavLink>

      <StyledLink exact to="/" activeClassName="active">
        Art salon
      </StyledLink>

      <StyledLink to="/festival" activeClassName="active">
        Festival
      </StyledLink>

      <StyledLink to="/contacts" activeClassName="active">
        Contacts
      </StyledLink>

      <StyledLink to="/courses" activeClassName="active">
        Education
      </StyledLink>
    </StyledHeader>
  );
}

const StyledLink = styled(NavLink)`
  ${tw`flex items-center justify-center`}

  padding: 12px 15px;
  background-color: #fff;

  color: #000;
  text-decoration: none;

  &.active {
    background-color: #F0DD60;
  }

  &:visited {
    color: #000;
  }
`;

const StyledHeader = styled.header`
  top: 0;
  border-top: 4px solid #bfa69a;
  background-color: #fff;

  ${tw`flex px-10 fixed w-full`}
`;