import tw from 'tailwind.macro';
import React from 'react';
import styled from 'styled-components';

export const Footer = () => (
  <StyledFooter>
    Copyright Vyacheslav Begidzhanov © 2020. All Rights Reserved.
  </StyledFooter>
);

const StyledFooter = styled.footer`
  ${tw`w-full`}

  border-top: 1px dotted #737373;
  padding: 5px 0;
  color: #6986A5;
  font-size: 13px;
  line-height: 175%;
  text-align: center;
`;