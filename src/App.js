import React from 'react';
import { Switch, Route, HashRouter } from 'react-router-dom';

import { Header } from './components/header/Header';
import { ArtSalon } from './pages/art-salon/ArtSalon';

import './App.css';
import { Book } from './pages/book/Book';
import { Slogan } from './pages/slogan/Slogan';
import { Footer } from './components/footer/Footer';
import { Courses } from './pages/courses/Courses';
import { Festival } from './pages/festival/Festival';
import { Contacts } from './pages/contacts/Contacts';

const handlePageChange = () => {
  window.scrollTo(0, 0);
}

function App() {
  return (
    <HashRouter>
      <div className="App">
        <Header />

        <Switch>
          <Route exact path="/" onEnter={handlePageChange}>
            <ArtSalon />
          </Route>

          <Route path="/festival" onEnter={handlePageChange}>
            <Festival />
          </Route>

          <Route path="/book" onEnter={handlePageChange}>
            <Book />
          </Route>

          <Route path="/slogan" onEnter={handlePageChange}>
            <Slogan />
          </Route>

          <Route path="/contacts" onEnter={handlePageChange}>
            <Contacts />
          </Route>

          <Route path="/courses" onEnter={handlePageChange}>
            <Courses />
          </Route>
        </Switch>

        <Footer />
      </div>
    </HashRouter>
  );
}

export default App;
