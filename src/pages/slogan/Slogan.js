import tw from 'tailwind.macro';
import React from 'react';
import styled from 'styled-components';

import image from './assets/3.png'
import { NavLink } from 'react-router-dom';

export const Slogan = () => {
  return (
    <StyledSlogan>
      <NavLink to="/">
        <img src={image} alt="ТО ЧТО НАВЕРХУ ПОДОБНО ТОМУ, ЧТО ВНИЗУ, А ТО ЧТО ВНИЗУ ПОДОБНО ТОМУ, ЧТО НАВЕРХУ."/>
      </NavLink>

      <StyledText>
        "ТО ЧТО НАВЕРХУ ПОДОБНО ТОМУ, ЧТО ВНИЗУ,<br />
        А ТО ЧТО ВНИЗУ ПОДОБНО ТОМУ, ЧТО НАВЕРХУ."
      </StyledText>
    </StyledSlogan>
  );
}

const StyledText = styled.div`
  color: #303F50;
  font-size: 37px;
  margin-top: 26px;
  font-weight: 100;
  line-height: 33px;
`;

const StyledSlogan = styled.div`
  ${tw`w-full h-full flex flex-col items-center justify-center`}
  min-height: 95vh;
`;