import React from 'react';
import tw from 'tailwind.macro';
import styled from 'styled-components';
import { FullscreenSingleCenterImage } from '../../core/styles/images';
import { BigWhiteTitle, TextCenter } from '../../core/styles/text';

import bookImage from './assets/book.png';
import crossImage from './assets/cross.jpg';
import { NavLink } from 'react-router-dom';
import { useScrollToTop } from '../../core/effects/use-scroll-top';

export const Book = () => {
  useScrollToTop();

  return (
    <StyledBook>
      <FullscreenSingleCenterImage image={crossImage} />

      <StyledMarginTop>
        <TextCenter>
          <BigWhiteTitle>СВЕТ</BigWhiteTitle>
          <BigWhiteTitle>БОЖЕСТВЕННЫЙ</BigWhiteTitle>
          <BigWhiteTitle>ДАНТЕ</BigWhiteTitle>
        </TextCenter>
      </StyledMarginTop>

      <StyledMarginTop>
        <BookImageComposition>
          <StyledBookImage src={bookImage} alt="book" />
        </BookImageComposition>

        <StyledArrowsRow>
          <StyledArrow to="/">
            &lt;
          </StyledArrow>
          <StyledArrow to="/slogan">
            &gt;
          </StyledArrow>
        </StyledArrowsRow>

      </StyledMarginTop>
    </StyledBook>
  );
}

const StyledArrowsRow = styled.div`
  display: flex;
  justify-content: center;
`;

const StyledArrow = styled(NavLink)`
  font-size: 40px;
  width: 80px;
  padding: 0 20px;
  background-color: #eff1f4;
  border-top-left-radius: 1em 5em;
  border-top-right-radius: 1em 5em;

  img {
    width: inherit;
  }

  color: #000 !important;

  &:visited {
    color: #000 !important;
  }
`;

const BookImageComposition = styled.div`
  ${tw`flex justify-center items-center`}
`;

const StyledMarginTop = styled.div`
  margin-top: 60px;
`;

const StyledBook = styled.div`
`;

const StyledBookImage = styled.img`
  margin: 20px 0;
`;