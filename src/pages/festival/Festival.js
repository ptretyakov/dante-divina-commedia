import tw from 'tailwind.macro';
import React from 'react';
import styled from 'styled-components';
import { ImageWithSignature } from '../../core/styles/images';
import { ImageSubtitle, BigWhiteTitle, Quote, TextSubtitle } from '../../core/styles/text';

import vasaImage from './assets/vasa.jpg';
import crossImage from './assets/cross.jpg';
import coverImage from './assets/cover.jpg';
import gobelenImage from './assets/gobelen.png';
import gobelenTitleImage from './assets/dante-light-title.png';
import { Parallax } from 'react-parallax';

export const Festival = () => {
  return (
    <StyledFestival>
      <StyledCoverImage src={coverImage} />

      <Parallax bgImage={vasaImage} style={{minHeight: '100vh'}} strength={500} bgImageStyle={{width: '100%', height: '100%'}}>
        <BigWhiteTitle>Божественная <br /> комедия</BigWhiteTitle>
          <ImageSubtitle>ART FESTIVAL</ImageSubtitle>
        <ImageSubtitle>August 18-20, 2021</ImageSubtitle>
      </Parallax>

      <StyledTextWrapper>
        <StyledDanteTitleImage src={gobelenTitleImage} />

        <ImageWithSignature
          withShadow
          image={gobelenImage}
          signature="Гобелен. 1998. 3,5м. - 4,5м. Автор: Вячеслав Бегиджанов"
        />

        <Quote
          text={'Мы  подошли к окраине обвала.\nГде груда скал под нашею пятой\nЕщё страшней  пучину открывала.'}
          signature='Данте Алигьери "Божественная комедия" - Ад, песнь одиннадцатая.'
        />

        <TextSubtitle>О фестивале</TextSubtitle>
        <p>
          Арт фестиваль <strong>"Ковчег"</strong> 2021 года будет посвящён юбилейным датам Данте Альгильери: 750 лет со дня рождения — 700 лет со дня смерти и в связи с этим, его творчеству — <strong>"ДАНТЕ БОЖЕСТВЕННЫЙ СВЕТ"</strong>.
          Фестиваль имеет целью собрать все возможные направления и стили в единый букет искусств. <strong>"Букет Искусств"</strong> объединит в звучание соборной красоты и постижения в <strong>"Ковчеге"</strong> под небом Франции, плывущем в парке замка Шардонно виды искусств. Будут приглашены виды и жанры искусства.
        </p>

        <br />

        <ul>
          {[
            'Живопись',
            'Графика',
            'Скульптура',
            'Музыка',
            'Театр',
          ].map((t, i) => <li key={`about-${i}`}>{i + 1}. {t}</li>)}
        </ul>

        <p>а также, все смежные виды.</p>

        <br />

        <p><strong>"Букет Искусств"</strong> —  это Гармония всех цветов и оттенков творческих усилий в прекрасном плавании под небом Франции.</p>
        <p>Также к участию фестиваля будут приглашены гости фестиваля — Гость фестиваля <strong>"Ковчег"</strong> - именной пригласительный билет, приглашающий известных искусствоведов, историков и теоретиков искусств, а также представителей различных музеев, галерей, меценатов и кураторов. Будет избрано жюри фестиваля, которое попробует собрать  этот букет в его соборном звучании и определить победителей Дантовского фестиваля искусств.  Определятся победители Дантовского фестиваля искусств.</p>
        <p>По итогам фестиваля планируется выпустить каталог — альбом <strong>"ДАНТЕ БОЖЕСТВЕННЫЙ СВЕТ"</strong>, а также:</p>
        
        <br />

        <p>1. Объявлен конкурс - <strong>"Книга художника"</strong>, в котором примут участие все желающие художники. <strong>"Книга художника"</strong>  это - а), - б),-  в).....</p>
        <p>2. Будет проведён <strong>"Благотворительный аукцион"</strong>, средства от которого будут переданы в лечебно — восстановительный центр <strong>"Ковчег"</strong> в городе Ли Ман. Франция, таким образом Искусство  вновь обретёт символ  Гуманизма  и Благочестия, а этот высокий смысл  был утрачен им в новое время.</p>

        <StyledManifestTitle>МАНИФЕСТ</StyledManifestTitle>

        <StyledManifstText>
          <p>"СВЕТ БОЖЕСТВЕННЫЙ ДАНТЕ" ИЗВЛЁК ИЗ БЕЗДНЫ НЕБЫТИЯ " ЧЁРНОГО КВАДРАТА"</p>

          <p>"БЕЛЫЙ КВАДРАТ" жизни вечной, определив противоположность и взаимоисключение ЭТОГО НРАВСТВЕННОГО СОБЫТИЯ.</p>

          <p>
            "ЧЁРНЫЙ КВАДРАТ" - это тьма бездны небытия
            "БЕЛЫЙ КВАДРАТ" - это свет жизни вечной
          </p>

          <p>
            "свет" - это хорошо <br />
            "тьма" - это плохо
          </p>

          <p>
            "Кто ходит днём, тот не спотыкается, потому что видит свет мира сего, а кто ходит ночью спотыкается, потому что нет света с ним.
            Ходите, пока есть СВЕТ, чтобы не объяла вас ТЬМА ; а ходящий во тьме не знает куда идёт"
          </p>

          <p>"Я ЕЗМЬ ВОСКРЕСЕНИЕ И ЖИЗНЬ"</p>
          <StyledManifestSignature>Eвангелие от Иоанна</StyledManifestSignature>

          <br /><br />

          <p>Утверждение через отрицание, объединение всех живущих на земле и страждущих света во тьме бездны "Чёрного квадрата" в стремлении к cвету "Божественного Белого квадрата", объединяет этот фестиваль, мы призываем всех к объединению и пусть факелом этого братства, возжелавших Света , будет этот Фестиваль "СВЕТА БОЖЕСТВЕННОГО ДАНТЕ", КОТОРЫЙ БУДЕТ ПРОВОДИТСЯ ЕЖЕГОДНО, распространяя СВЕТ БОЖЕСТВЕННЫЙ и объединяя людей в их стремлении быть причастными этому СВЕТУ.</p>
          <StyledManifestSignatureSmall>Вячеслав Бегиджанов</StyledManifestSignatureSmall>
        </StyledManifstText>

        <ImageWithSignature
          image={crossImage}
          signature={`Скульптор - Георгий Франглян. "Распятие"`}
          withoutBorder
        />

        <StyledGoogleMapIframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1380419.6247915134!2d0.09378404714710199!3d48.11745668612875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e29693d3343fe9%3A0xee35d042099563a9!2z0KjQsNGA0LTQvtC90ZEsIDcyMjIwINCh0LXQvS3QkdGM0LXQty3QsNC9LdCR0LXQu9C10L0sINCk0YDQsNC90YbQuNGP!5e0!3m2!1sru!2sru!4v1603383001896!5m2!1sru!2sru"
          width="600"
          height="450"
          tabindex="0"
          frameborder="0"
          aria-hidden="false"
          allowfullscreen="">
        </StyledGoogleMapIframe>
      </StyledTextWrapper>
    </StyledFestival>
  );
}

const StyledDanteTitleImage = styled.img`
  width: 100%;
`;

const StyledCoverImage = styled.img`
  min-height: 95vh;
  margin-bottom: 100px;
  box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
`;

// const StyledParallax = styled(Parallax)`
//   min-height: 100vh;
// `;

const StyledGoogleMapIframe = styled.iframe`
  ${tw`w-full pb-10`}
`;

const StyledManifestSignatureSmall = styled.span`
  color: black !important;
  font-size: 24pt !important;
  font-style: italic;
  font-weight: normal !important;
`;

const StyledManifestSignature = styled.span`
  color: black !important;
  font-size: 34pt !important;
  font-style: italic;
  font-weight: normal !important;
`;

const StyledManifstText = styled.div`
  color: red;
  font-size: 36pt;
  font-weight: bold;

  p {
    margin: 34px 0px !important;
    line-height: 55px;
  }
`;

const StyledManifestTitle = styled.div`
  ${tw`mt-20 mb-5`}
  font-size: 101px;
  text-align: center;
  line-height: 101px;
  font-weight: 500;
  text-transform: uppercase;
`;

const StyledFestival = styled.div``;

const StyledTextWrapper = styled.div`
  width: 70%;
  margin: auto;
  font-size: 18pt;
  text-align: justify;
  line-height: 40px;

  p {
    ${tw`mt-2`}
  }

  strong {
    font-weight: bold;
  }

  ${tw`mt-10`}
`;