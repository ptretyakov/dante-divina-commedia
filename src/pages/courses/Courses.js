import React from 'react';
import tw from 'tailwind.macro';
import styled from 'styled-components';

import michContrastImage from './assets/images/michelangelo-contrast.jpg';

export const Courses = () => {
  return (
    <StyledCourses>
      <TopWrapper>
        <StyledLeftSide>
          <StyledLeftText>
            "ПЛАСТИЧЕСКАЯ НЕРАЗРЫВНОСТЬ НРАВСТВЕННОСТИ ПЛАСТИЧЕСКОГО СОБЫТИЯ"
          </StyledLeftText>
        </StyledLeftSide>
        
        <StyledRightSide>
          <img src={michContrastImage} alt="" />
          <StyledVinchiSignature>Распятие. Рисунок. 1541. Микеланджело Буонарроти. Британский музей. Лондон</StyledVinchiSignature>
        </StyledRightSide>
      </TopWrapper>

      <StyledTextCenter>
        <StyledTextTitle>Даты курса</StyledTextTitle>
          <p>Каждый мастер-класс длится 14 дней.</p>

          <StyledTextTitle>2020 год</StyledTextTitle>
          <ul>
            {[
              '6-19 июля',
              '20 июля - 2 августа',
              '3-16 августа',
              '17-30 августа',
            ].map((t, i) => <li key={`salon-dates-${i}`}>{t}</li>)}
          </ul>

          <StyledTextTitle>Стоимость</StyledTextTitle>
          <p>Проживание в номере на двоих человек в корпусе с мастерскими: 1400 Евро за 14 дней</p>
          <p>Проживание в номере на двоих в замке: 2100 Евро за 14 дней</p>
          <p>Забронировать участие в мастер-классе вы можете на странице Контакты.</p>

          <StyledTextTitle>Music</StyledTextTitle>
          <p>
            Рождение трагедии из духа музыки
            На время проведения мастер-классов пластического рисунка и не только, мы приглашаем музыкантов.Концерты проходят в камерной обстановке в живописных интерьерах замка.
          </p>
      </StyledTextCenter>
    </StyledCourses>
  );
}

const StyledVinchiSignature = styled.div`
  font-size: 12pt;
`;

const StyledTextCenter = styled.div``;

const StyledTextTitle = styled.h3`
  font-size: 18pt;
  font-weight: 600;
  ${tw`mt-3 mb-7`}
`;

const StyledLeftSide = styled.div`
  width: 50%;
  ${tw`flex justify-center items-center p-20`}
`;

const StyledRightSide = styled.div`
  width: 50%;

  img {
    height: 85vh !important;
    box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
  }
`;

const StyledLeftText = styled.div`
  text-align: center;
  color: #303F50;
  font-size: 37px;
  margin-top: 26px;
  font-weight: 100;
  line-height: 50px;
`;

const TopWrapper = styled.div`
  ${tw`flex justify-center pt-20`};
`;

const StyledCourses = styled.div`
  margin: auto;
  width: 70%;
  font-size: 18pt;
  text-align: justify;
  line-height: 40px;

  img {
    height: 90vh;
  }
`;