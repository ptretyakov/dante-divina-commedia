import tw from 'tailwind.macro';
import React from 'react';
import styled from 'styled-components';

export const Contacts = styled((props) => <div {...props}>
  Get in touch with us:
  <a href="mailto:chardonneux@gmail.com">chardonneux@gmail.com</a>

  <StyledGoogleMapIframe
    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1380419.6247915134!2d0.09378404714710199!3d48.11745668612875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e29693d3343fe9%3A0xee35d042099563a9!2z0KjQsNGA0LTQvtC90ZEsIDcyMjIwINCh0LXQvS3QkdGM0LXQty3QsNC9LdCR0LXQu9C10L0sINCk0YDQsNC90YbQuNGP!5e0!3m2!1sru!2sru!4v1603383001896!5m2!1sru!2sru"
    width="600"
    height="450"
    tabindex="0"
    frameborder="0"
    aria-hidden="false"
    allowfullscreen="">
  </StyledGoogleMapIframe>
</div>)`
  ${tw`flex flex-col items-center justify-center h-full m-auto`}

  width: 70%;
  font-size: 18pt;
`;

const StyledGoogleMapIframe = styled.iframe`${tw`w-full pb-10 mt-20`}`;