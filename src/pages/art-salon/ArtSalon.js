import tw from 'tailwind.macro';
import React from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

import { useScrollToTop } from '../../core/effects/use-scroll-top';

import hatoImage from './assets/hato.jpg';
import hallImage from './assets/hall.jpg';
import wordImage from './assets/word1.jpg';
import musicImage from './assets/music.jpg';
import imageImage from './assets/image.jpg';
import backgroundImage from './assets/background.jpg';
// import logoImage from '../../components/header/assets/logo.jpg';

const salonParts = [
  { image: imageImage, title: 'Image', subtitle: 'Мастер-класс пластического рисования' },
  { image: musicImage, title: 'Music', subtitle: 'Рождение трагедии из духа музыки. Концерты в замке' },
  { image: wordImage, title: 'Word', subtitle: 'Курс образование словесно- пространственных отношений.' },
];

export const ArtSalon = () => {
  useScrollToTop();

  return (
    <div>
      <BackgrounImageWrapper image={backgroundImage}>
        <StyledH1>ART SALON</StyledH1>
        <StyledH3>WORD, MUSIC, IMAGE.</StyledH3>
      </BackgrounImageWrapper>

      <SalonPartsWrapper>
        {salonParts.map((part, i) => (
          <StyledPartBlock key={`salon-parts-${i}`}>
            <StyledPartImageBlock image={part.image} />
            <StyledPartTitle>{part.title}</StyledPartTitle>
            <StyledPartSubtitle>{part.subtitle}</StyledPartSubtitle>
          </StyledPartBlock>
        ))}
      </SalonPartsWrapper>

      <StyledTextWrapper>
        <p>
          Воссоздавая литературно — музыкальный салон в здании, которое специально построено для этих мероприятий во Франции и, учитывая просветительскую специфику этого вида творческой активности, мы выстроили концепцию подачи темы "Слово + Музыка + Изображение" в игровом контексте, целью которого было показать взаимосвязь и перетекание сюжетно- пространственной драматургии жанров. Этот салон начал свою деятельность в 2015 году и с тех пор работал, также, как музей,

          <a href="http://chardonneux-novosti.blogspot.com/" target="blank">
            &nbsp;принимая образовательные экскурсии.
          </a>
        </p>

        <p>
        Салон начал свою деятельность в 2015 году в год 750-летия со дня рождения ДАНТЕ АЛИГЬЕРИ и в 2021 году, в год 700-летия со дня смерти поэта предполагает провести Международный Фестиваль искусств на тему "Божественной комедии" Данте. Салон - "Русская Элегия", является заставкой просветительской и образовательной программы, которую мы начинаем, как подготовительный этап к юбилейному международному фестивалю искусств - "ДАНТЕ" в 2021 году.
        </p>

        <StyledTextImage src={hallImage} />

        <StyledTextTitle>Музыкально литературный салон - "РУССКАЯ ЭЛЕГИЯ." Вступительная часть -"СОПРИЧАСТНОСТЬ".</StyledTextTitle>

        <ul>
          {[
            'СЛОВО. Курс образование словесно - пространственных отношений',
            'МУЗЫКА. Рождение трагедии из духа музыки. ',
            'ИЗОБРАЖЕНИЕ'
          ].map((t, i) => <li key={`text-parts-${i}`}>{i + 1}. {t}</li>)}
        </ul>

        <p>
          a. Основной курс — курс пластического рисования (пространственные отношения, как нравственность пластического события). Лекцию читает автор проекта художник Вячеслав Бегиджанов, уединившийся в своём замке в центре Франции,
          одинокий мыслитель уединившийся в своём замке и<NavLink to="/book"> &gt;&gt; постигает  Данте Божествнный Свет</NavLink>
        </p>
        <p>
          b. Основной курс. Пространственные отношения, как нравственность пластического события. Рисунок обнаженной модели и живопись на тему стихов Данте или на свободную тему.
        </p>
        <p>
          По результатам мастер-класса художникам будет предложено принять участие в арт-фестивале ARK, который пройдет на территории замка летом 2021 года.
        </p>

        <StyledTextTitle>Мастерские и проживание</StyledTextTitle>
        <p>Каждому художнику предлагается отдельная мастерская с большими окнами для рисования. Материалы для работы (краски, холсты, бумага) художники привозят с собой.</p>
        <p>Для проживания предлагается два варианта: номер на двоих человек в корпусе с мастерскими или номер на двоих в замке.</p>
        <p>В день заезда мы организуем автобус от ближайшей железнодорожной станции. Так же вы можете самостоятельно заказать такси (3 км).</p>
        <p>Питание на общей кухне 40 кв.м. рядом с мастерскими. Готовим сами. Продукты закупаются с учетом ваших предпочтений.</p>

        <StyledTextTitle>Music</StyledTextTitle>
        <p>
          Рождение трагедии из духа музыки
          На время проведения мастер-классов пластического рисунка и не только, мы приглашаем музыкантов.Концерты проходят в камерной обстановке в живописных интерьерах замка.
        </p>

        <StyledTextTitle>Word</StyledTextTitle>
        <p>
          Курс образование словесно- пространственных отношений.<br />
          Гостями нашего Салона бывают не только музыканты, но писатели и поэты. Интерьеры замка располагают к осознанному восприятию слова.
        </p>

        <p>
          <a href="http://chardonneux-novosti.blogspot.com/search?updated-max=2011-10-01T21:53:00%2B02:00&max-results=7&start=2&by-date=false" target="blank">
            • Хроника академии Шардонно &gt;&gt;
          </a>
        </p>

        <StyledTextImage src={hatoImage} />

        <StyledGoogleMapIframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1380419.6247915134!2d0.09378404714710199!3d48.11745668612875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e29693d3343fe9%3A0xee35d042099563a9!2z0KjQsNGA0LTQvtC90ZEsIDcyMjIwINCh0LXQvS3QkdGM0LXQty3QsNC9LdCR0LXQu9C10L0sINCk0YDQsNC90YbQuNGP!5e0!3m2!1sru!2sru!4v1603383001896!5m2!1sru!2sru"
          width="600"
          height="450"
          tabindex="0"
          frameborder="0"
          aria-hidden="false"
          allowfullscreen="">
        </StyledGoogleMapIframe>
      </StyledTextWrapper>
    </div>
  );
}

const StyledGoogleMapIframe = styled.iframe`
  ${tw`w-full pb-10`}
`;

const StyledTextTitle = styled.h3`
  font-size: 18pt;
  font-weight: 600;
  ${tw`mt-10 mb-7`}
`;

const StyledTextImage = styled.img`
  ${tw`mt-10 mb-10`}
  width: 100%;
  border: 1px solid #000;
`

const StyledTextWrapper = styled.div`
  width: 70%;
  margin: auto;
  font-size: 18pt;
  text-align: justify;
  line-height: 40px;

  p {
    ${tw`mt-2`}
  }
`;

const StyledPartSubtitle = styled.div`
  color: #989898;
  font-size: 16px;
`;

const StyledPartTitle = styled.div`
  font-size: 22px;
`;

const StyledPartBlock = styled.div`
  ${tw`flex flex-col`}

  width: 230px;
`;

const StyledPartImageBlock = styled.div`
  width: 100%;
  height: 145px;
  border: 1px solid #000;
  background-size: cover;
  background-image: url(${p => p.image});
`;

const SalonPartsWrapper = styled.div`
  ${tw`flex mt-10 mb-10`}

  justify-content: space-evenly;
`;

const StyledH1 = styled.h1`
  color: #fff;
  font-size: 151px;
  text-shadow: 0px 0px 10px #000000;
`;

const StyledH3 = styled.h3`
  color: #fff;
  font-size: 30px;
  margin-top: 13px;
  text-shadow: 0px 0px 10px #000000;
`;

const BackgrounImageWrapper = styled.div`
  height: calc(100vh - 140px);
  padding-top: 140px;
  background-size: cover;
  background-image: url(${p => p.image});
`;