# ==================== Build ========================
FROM node:alpine as front
WORKDIR /app
COPY . .
RUN ls -al
RUN yarn build

# ================ Deploy =========================
FROM nginx:alpine

RUN rm /etc/nginx/conf.d/default.conf
COPY ./nginx-configs/nginx.conf /etc/nginx/conf.d
COPY ./public/50x.html /usr/share/nginx/html
COPY ./public/404.html /usr/share/nginx/html
COPY --from=front /app/build/ /usr/share/nginx/html